<?php 
namespace Bss\SubCategory\Block;
/**
 * 
 */
class View extends \Magento\Catalog\Block\Category\View
{
    protected $categoryFactory;
	public function __construct(\Magento\Framework\View\Element\Template\Context $context,
        \Magento\Catalog\Model\Layer\Resolver $layerResolver,
        \Magento\Framework\Registry $registry,
        \Magento\Catalog\Helper\Category $categoryHelper,
        array $data = [],
        \Magento\Catalog\Model\CategoryFactory $categoryFactory
    ){
        parent::__construct($context,$layerResolver,$registry,$categoryHelper,$data);
        $this->categoryFactory = $categoryFactory;
    }
    public function getSubCategori($id){
        $sub=$this->categoryFactory->create();
        $sub = $sub->load($id);
        return $sub;
    }
}




 ?>