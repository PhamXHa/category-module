<?php 
namespace Bss\SubCategory\Block;
/**
 * 
 */
class Topmenu extends \Magento\Theme\Block\Html\Topmenu
{
	
	public function _addSubMenu($child, $childLevel, $childrenWrapClass, $limit)
    {
        $html = '';
        if (!$child->hasChildren()) {
            return $html;
        }

        $colStops = null;
        if ($childLevel == 0 && $limit) {
            $colStops = $this->_columnBrake($child->getChildren(), $limit);
        }

        $html .= '<ul class="level' . $childLevel . ' ' . $childrenWrapClass . '">';
        $html .='<li class="<?=($childLevel+1)?>"  >'.'<a href="' . $child->getUrl() . '" ><span>' . $this->escapeHtml("View All " .
                $child->getName()) . '</span></a></li>';
        $html .= $this->_getHtml($child, $childrenWrapClass, $limit, $colStops);
        $html .= '</ul>';

        return $html;
    }
}




 ?>